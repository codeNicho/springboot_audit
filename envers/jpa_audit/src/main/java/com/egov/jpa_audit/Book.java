package com.egov.jpa_audit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@AllArgsConstructor // still lombok feature for generating constructors e.t.c.
@NoArgsConstructor // still lombok
@Data // still lombok in action, generates getters and setters and toString

@Audited
public class Book {
    @Id
    @GeneratedValue
    private int id;

    private String name;
    private int pages;
}
