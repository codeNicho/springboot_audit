package com.egov.jpa_audit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.envers.repository.support.EnversRevisionRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(path = "api")
@RestController
@SpringBootApplication
/*Since we are using envers we need to provide it's bean factory with the EnableJpaRepositories below
 FactoryBean creating RevisionRepository instances*/
@EnableJpaRepositories(repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class)

public class JpaAuditApplication {

	@Autowired
	private BookRepository bookRepository;

	@PostMapping("/savebook")
	public Book saveBook(@RequestBody Book book){
		return bookRepository.save(book);
	}

	@PutMapping("/update/{id}/{pages}")
	public String updateBook(@PathVariable int id, @PathVariable int pages){
		Book book = bookRepository.findById(id).get();
		book.setPages(pages);
		bookRepository.save(book);
		return "Book Updated";
	}

	@DeleteMapping("/delete/{id}")
	public String deleteBook(@PathVariable int id){
		bookRepository.deleteById(id);
		return "Book deleted!";
	}



	public static void main(String[] args) {
		SpringApplication.run(JpaAuditApplication.class, args);
	}

	@GetMapping("/getinfo/{id}")
	public void getInfo(@PathVariable int id){
		System.out.println(bookRepository.findLastChangeRevision((id)));
		/*Programmatically getting the Revision history by using the */
	}

	@GetMapping("/revisions/{id}")
	public void getRevisions(@PathVariable int id){
		System.out.println(bookRepository.findRevisions((id)));
		/*Programmatically getting the Revision history by using the */
	}


	@GetMapping("{id}")
	public Optional<Book> viewBook(@PathVariable("id") int id){
		return bookRepository.findById(id);
	}

	@GetMapping("/all")
	public List<Book> all(){
		return bookRepository.findAll();
	}

}
